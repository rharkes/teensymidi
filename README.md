# TeensyMidi

A simple midi controller board for teensy 3.2. It uses eight analog multiplexers (74HC4051D) for a combined 64-channel input.
For use with a hauptwerk pedal board. 